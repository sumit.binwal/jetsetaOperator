//
//  AppWebHandler.h
//  Tabco App
//
//  Created by Santosh on 21/12/15.
//  Copyright © 2015 Vaibhav Khatri. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark -
#pragma mark Internet Check Bool
FOUNDATION_EXPORT BOOL isInternetAvailabel;

#pragma mark -
#pragma mark HttpMethodType Enum
typedef NS_ENUM(NSInteger,HttpMethodType)
{
    HttpMethodTypeGet = 0,// GET
    HttpMethodTypePost // POST
};

#pragma mark -
#pragma mark SuccessHandler Block
typedef void(^DataTaskSuccessfullHandler)(NSData *data, NSDictionary *dictionary);
#pragma mark -
#pragma mark ErrorHandler Block
typedef void(^DataTaskFailureHandler)(NSError *error);

#pragma mark -
#pragma mark -
#pragma mark CLASS INTERFACE
@interface AppWebHandler : NSObject

#pragma mark -
#pragma mark Class Functions
+ (AppWebHandler *)sharedInstance;

#pragma mark -
#pragma mark Instance Functions
- (void)fetchDataFromUrl:(NSURL *)url httpMethod:(HttpMethodType)httpMethodType parameters:(NSDictionary *)parameters shouldDeSerialize:(BOOL)deSerialize success:(DataTaskSuccessfullHandler)successHandler failure:(DataTaskFailureHandler)failureHandler;


@end
