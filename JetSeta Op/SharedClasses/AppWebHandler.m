//
//  AppWebHandler.m
//  Tabco App
//
//  Created by Santosh on 21/12/15.
//  Copyright © 2015 Vaibhav Khatri. All rights reserved.
//

#import "AppWebHandler.h"
#import "Reachability.h"

#pragma mark -
#pragma mark Internet Check Bool
BOOL isInternetAvailabel;

#pragma mark -
#pragma mark CLASS EXTENSION
@interface AppWebHandler ()<NSURLSessionDelegate>

#pragma mark -
@property(nonatomic)Reachability *internetRechability;
@property(nonatomic,strong)NSURLSession *httpSession;
#pragma mark -
- (NSMutableURLRequest *)createGetRequestWithUrl:(NSURL *)url;
- (NSMutableURLRequest *)createPostRequestWithUrl:(NSURL *)url andParmaters:(NSDictionary *)dictionary;

@end
#pragma mark -

#pragma mark -
#pragma mark CLASS IMPLEMENTATION
@implementation AppWebHandler

#pragma mark -
#pragma mark Shared Handler
+(AppWebHandler *)sharedInstance
{
    static AppWebHandler *_webHandler;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _webHandler = [[self alloc]init];
    });
    return _webHandler;
}

#pragma mark -
#pragma mark Init
-(instancetype)init
{
    if (self = [super init])
    {
        NSURLSessionConfiguration *_configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _httpSession = [NSURLSession sessionWithConfiguration:_configuration];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
        
        self.internetRechability = [Reachability reachabilityForInternetConnection];
        [self.internetRechability startNotifier];
        
        [self checkNetworkStatus:nil];
        
    }
    return self;
}


#pragma mark -
#pragma mark Data Fetch Method
- (void)fetchDataFromUrl:(NSURL *)url httpMethod:(HttpMethodType)httpMethodType parameters:(NSDictionary *)parameters shouldDeSerialize:(BOOL)deSerialize success:(DataTaskSuccessfullHandler)successHandler failure:(DataTaskFailureHandler)failureHandler
{
    // Created initial task with nil value
    NSURLSessionDataTask *dataTask = nil;
    // Created initial request with nil value
    NSMutableURLRequest *urlRequest = nil;
    
    // Case for Http Method used for data fetch
    switch (httpMethodType)
    {
        case HttpMethodTypeGet:// Get http method
        {
            // Calling method "createGetRequestWithUrl:" which will return new instance of 'NSMutableURLRequest' which will be used for Get functionality.
            urlRequest = [self createGetRequestWithUrl:url];
        }
            break;
            
        case HttpMethodTypePost:// Post http method
        {
            // Calling method "createPostRequestWithUrl: andParmaters:" which will return new instance of 'NSMutableURLRequest' which will be used for Post functionality.
            urlRequest = [self createPostRequestWithUrl:url andParmaters:parameters];
        }
            break;
            
        default:
            break;
    }
    
    // Using the single session created, to make and start a new task of type 'NSURLSessionDataTask'. Request created above is passed as a paramater to perform task, along with completion handler block which return data(if no error is found) and error(if some error encountered).
    dataTask = [_httpSession dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            if (error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet ){
            }
            // Checking for error, if there is any
            if (error)
            {
                // If there is error, then checking the condition if its error handling block is nil.
                if (!failureHandler)
                {
                    // Nil, so returning from the method, user does not posted with anything.
                    return;
                }
                else
                {
                    // User is posted with the error.
                    failureHandler(error);
                    return;
                }
            }
            // If no completion handler, then return.
            if (!successHandler) {
                return;
            }
            
            NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
            
            // Bool to check whether develoepr wants result in deserialized format ,i.e., in foundation object
            if (deSerialize)
            {
                // Converting data into NSDictionary object.
                NSError *deSerializeError;
                NSDictionary *deSerializedDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&deSerializeError];
                
                // If error
                if (deSerializeError)
                {
                    // If there is error, then checking the condition if its error handling block is nil.
                    if (!failureHandler)
                    {
                        return;
                    }
                    else
                    {
                        // User is posted with the error.
                        failureHandler(deSerializeError);
                        return;
                    }
                }
                else
                {
                    // Sending data as nil and dictionary object because 'deSerialize is TRUE'.
                    successHandler(nil,deSerializedDictionary);
                }
            }
            else
            {
                // Sending data and dictionary as nil object because 'deSerialize is FALSE'.
                successHandler(data,nil);
            }

            
            
        });
    }];
    
    // Start the data fetch process.
    [dataTask resume];
}

#pragma mark -
#pragma mark Get Request Generation Method
- (NSMutableURLRequest *)createGetRequestWithUrl:(NSURL *)url
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setHTTPMethod:@"GET"];
    
    return urlRequest;
}

#pragma mark -
#pragma mark Post Request Generation Method
- (NSMutableURLRequest *)createPostRequestWithUrl:(NSURL *)url andParmaters:(NSDictionary *)dictionary
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setHTTPMethod:@"POST"];
    
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error];
    NSString *str = [[NSString alloc]initWithData:postData encoding:NSUTF8StringEncoding];
    str = [str stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    postData = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@",[[NSString alloc]initWithData:postData encoding:NSUTF8StringEncoding]);
    if (error)
    {
        NSString *postBody = [NSString string];
        
        for (NSString *key in dictionary.allKeys)
        {
            id anyObject = dictionary[key];
            postBody = [postBody stringByAppendingString:[NSString stringWithFormat:@"%@=%@ ",key,anyObject]];
        }
        postBody = [postBody stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [urlRequest setHTTPBody:[postBody dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else
    {
        [urlRequest setHTTPBody:postData];
    }
    return urlRequest;
}

#pragma mark -
#pragma mark Internet Handling
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus internetStatus = [self.internetRechability currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            NSLog(@"The internet is down.");
            isInternetAvailabel = NO;
            
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"The internet is working via WIFI.");
            isInternetAvailabel = YES;
            
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"The internet is working via WWAN.");
            isInternetAvailabel = YES;
            
            break;
        }
    }
}


@end
