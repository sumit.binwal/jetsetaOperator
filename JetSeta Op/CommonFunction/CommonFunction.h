//
//  CommonFunction.h
//  GiffPlugApp
//
//  Created by Kshitij Godara on 08/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "SWRevealViewController.h"

@interface CommonFunction : NSObject

#pragma mark - hide/unhide navigation bar

+(NSString*)convertCustomeTime:(NSString *)currentDateString;
+(NSString*)convertCustomeDate:(NSString *)currentDateString;
+(void) setNavigationBar:(UINavigationController*)navController;
+(void)hideNavigationBarFromController:(UIViewController *)controller;
+(void)unHideNavigationBarFromController:(UIViewController *)controller;


#pragma mark - Set RootView Controller
+(void)changeRootViewController:(BOOL)isTabBar aircraftPublish:(BOOL)aircraft charterPublish:(BOOL)charter;

+(void)registerForRemoteNotification;
#pragma mark - Textfield Methods
+ (BOOL)isValueNotEmpty:(NSString*)aString;

+(NSString *)trimSpaceInString:(NSString *)mainstr;
+ (BOOL)isValidMobileNumber:(NSString*)number;
+(BOOL)IsValidEmail:(NSString *)checkString;
+ (NSString *)showAlertMsgWIthTextField:(UITextField *)txtFld;


+(NSString *) convertTimeStampToTime :(NSDate *) date;
#pragma mark - Shows UIAlerts
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController useAsDelegate:(BOOL)isDelegate dismissBlock:(void(^)())dismissBlock;


+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate;

+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg BtnTitle:(NSString*)btnName withDelegate:(id)delegate;

+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg;

+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate withTag:(int)tag;


#pragma mark - Reachablity Check 
+(BOOL) reachabiltyCheck;
+(BOOL)reachabilityChanged:(NSNotification*)note;


#pragma mark - Show n Hide Progress HUD
+ (void)removeActivityIndicator;

+ (void)showActivityIndicatorWithText:(NSString *)text;
+ (void)showActivityIndicatorWithWait;
+ (void)removeActivityIndicatorWithWait;
//+(void)navigateToTabBarController;


+(NSString *) convertTimeStampToDate :(NSDate *) date;
+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize;

+(void)setRoundedView:(UIView *)roundedView toDiameter:(float)newSize;

+(BOOL)isArivalDateIsComesAfterDepartureDate:(NSString *)strArrival departureDate:(NSString *)strDeparture;


@end
