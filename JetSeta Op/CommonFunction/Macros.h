//
//  Macros.h
//  GiffPlugApp
//
//  Created by Kshitij Godara on 08/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "AppDelegate.h"
#import <Foundation/Foundation.h>


#define IS_HEIGHT_GTE_568 [[UIScreen mainScreen ] bounds].size.height >= 568.0f
#define IS_IPHONE_5 (IS_HEIGHT_GTE_568 )
#define IS_IPHONE6 [[UIScreen mainScreen ] bounds].size.width >= 375.0f
#define IS_IPHONE6PLUS [[UIScreen mainScreen ] bounds].size.width >= 414.0f
#define IS_RETINA ([[UIScreen mainScreen]scale] == 2.0)
#define IS_VERSION_GREATER7  [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0

#define IS_VERSION_GREATER8  [[[UIDevice currentDevice] systemVersion] floatValue] > 8.0

#define FIX_LAY_OUT_ISSUE      if([self respondsToSelector:@selector(edgesForExtendedLayout)])self.edgesForExtendedLayout=UIRectEdgeNone;

#define SCREEN_WIDTH [[UIScreen mainScreen ] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen ] bounds].size.height

#define APPDELEGATE    ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define NSUSERDEFAULTS [NSUserDefaults standardUserDefaults]
#define NSUserDefaultss *userDefaults;
#define SCREEN_XScale [[UIScreen mainScreen ] bounds].size.width/320
#define SCREEN_YScale [[UIScreen mainScreen ] bounds].size.height/568

#define SCREEN_XScale_IPad [[UIScreen mainScreen ] bounds].size.width/768
#define SCREEN_YScale_IPad [[UIScreen mainScreen ] bounds].size.height/1024

#define IPAD     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

#define kAPPFOLDERNAME @"GiffFolder"
#define kGIFFPARTSFOLDER @"GiffParts"

#define kGIFFPARTSFOLDER @"GiffParts"

#define kLOGINUSERNAME @"username"
#define kLOGINPASSWORD @"password"
#define kISCATEGORYSUBMITTED @"hasCategory"
#define kISFOLLOWINGSUBMITTED @"hasFollowing"
#define kFOLLOWINGCATEGORY @"followingCategory"
#define kUSERTOKEN @"token"
#define kUSERNAME @"username"
#define kTUTORIALVISITED @"isTutorialScreenVisited"
#define kPUBLISH_AIRCRAFT @"is_publish_aircraft"
#define kOperatorName @"operatorName"
#define kPUBLISH_CHARTER @"is_publish_charter"
#define kPUBLISH_COUNT @"is_publish_count"


@interface Macros : NSObject

@end
