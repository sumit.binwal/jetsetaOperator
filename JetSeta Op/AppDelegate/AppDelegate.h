//
//  AppDelegate.h
//  JetSeta Op
//
//  Created by Sumit Sharma on 12/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>
{
    CLLocation *currentLocation;
}
@property(nonatomic,strong)UINavigationController *tabVC2;
@property (strong, nonatomic)IBOutlet UIWindow *window;
@property(nonatomic,strong)UITabBarController *mainTapBarCntroller;
@property(nonatomic,strong)UINavigationController *navigationController;


@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) CLLocationManager             *locationManager;
@property (nonatomic, assign) CLLocationCoordinate2D currentLocationCoordinate;
@property(nonatomic,strong)SWRevealViewController *revealController;
-(void)changeRootViewController:(BOOL)isTabBar aircraftPublish:(BOOL)aircraft charterPublish:(BOOL)charter;
@property (nonatomic, strong) NSMutableArray *arrCountryCode;
+(AppDelegate*)getSharedInstance;
-(void)showLocationEnableAlert;
@end

