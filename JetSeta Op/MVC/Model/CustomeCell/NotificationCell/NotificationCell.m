//
//  NotificationCell.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 25/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "NotificationCell.h"

@implementation NotificationCell

- (void)awakeFromNib {
    // Initialization code
    
//    @property (weak, nonatomic) IBOutlet UILabel *lblAdminMsg;
//    
//    

//    @property (strong, nonatomic) IBOutlet UILabel *lblAIrcraftName;
//    @property (strong, nonatomic) IBOutlet UILabel *lblAircraftType;






    
    if (IPAD) {
//        17.14.23
        [self.lblDate setFont:[UIFont fontWithName:self.lblDate.font.fontName size:14.0f]];
        [self.lblTime setFont:[UIFont fontWithName:self.lblTime.font.fontName size:14.0f]];
        
        [self.lblUserCount setFont:[UIFont fontWithName:self.lblUserCount.font.fontName size:17.0f]];
        [self.lblDepartureTime setFont:[UIFont fontWithName:self.lblDepartureTime.font.fontName size:17.0f]];
        [self.lblDepartureDate setFont:[UIFont fontWithName:self.lblDepartureDate.font.fontName size:17.0f]];
        
        [self.lblDestination setFont:[UIFont fontWithName:self.lblDestination.font.fontName size:17.0f]];
        [self.lblSource setFont:[UIFont fontWithName:self.lblSource.font.fontName size:17.0f]];
        [self.lblAIrcraftName setFont:[UIFont fontWithName:self.lblAIrcraftName.font.fontName size:17.0f]];
        [self.lblAircraftType setFont:[UIFont fontWithName:self.lblAircraftType.font.fontName size:17.0f]];
        
        [self.lblBookType setFont:[UIFont fontWithName:self.lblBookType.font.fontName size:23.0f]];
        
        [self.lblFor setFont:[UIFont fontWithName:self.lblFor.font.fontName size:17.0f]];
        [self.lblTo setFont:[UIFont fontWithName:self.lblTo.font.fontName size:17.0f]];
        [self.lblAt setFont:[UIFont fontWithName:self.lblAt.font.fontName size:17.0f]];
        [self.lblFrom setFont:[UIFont fontWithName:self.lblFrom.font.fontName size:17.0f]];
        
        [self.lblAdminMsg setFont:[UIFont fontWithName:self.lblAdminMsg.font.fontName size:17.0f]];
        [self.lblArrivalDate setFont:[UIFont fontWithName:self.lblArrivalDate.font.fontName size:17.0f]];
        [self.lblPriceunit setFont:[UIFont fontWithName:self.lblPriceunit.font.fontName size:17.0f]];
        [self.lblDepartureDatee setFont:[UIFont fontWithName:self.lblDepartureDatee.font.fontName size:17.0f]];

    }
    else
    {
        [self.lblDate setFont:[UIFont fontWithName:self.lblDate.font.fontName size:8.0f*SCREEN_XScale]];
        [self.lblTime setFont:[UIFont fontWithName:self.lblTime.font.fontName size:8.0f*SCREEN_XScale]];
        [self.lblUserCount setFont:[UIFont fontWithName:self.lblUserCount.font.fontName size:10.0f*SCREEN_XScale]];
        [self.lblDepartureTime setFont:[UIFont fontWithName:self.lblDepartureTime.font.fontName size:10.0f*SCREEN_XScale]];
        [self.lblDepartureDate setFont:[UIFont fontWithName:self.lblDepartureDate.font.fontName size:10.0f*SCREEN_XScale]];
        
        [self.lblArrivalDate setFont:[UIFont fontWithName:self.lblArrivalDate.font.fontName size:10.0f*SCREEN_XScale]];
        [self.lblPriceunit setFont:[UIFont fontWithName:self.lblPriceunit.font.fontName size:10.0f*SCREEN_XScale]];
        [self.lblDepartureDatee setFont:[UIFont fontWithName:self.lblDepartureDatee.font.fontName size:10.0f*SCREEN_XScale]];
        
        [self.lblDestination setFont:[UIFont fontWithName:self.lblDestination.font.fontName size:10.0f*SCREEN_XScale]];
        [self.lblSource setFont:[UIFont fontWithName:self.lblSource.font.fontName size:10.0f*SCREEN_XScale]];
        [self.lblAIrcraftName setFont:[UIFont fontWithName:self.lblAIrcraftName.font.fontName size:10.0f*SCREEN_XScale]];
        [self.lblAircraftType setFont:[UIFont fontWithName:self.lblAircraftType.font.fontName size:10.0f*SCREEN_XScale]];
        
        [self.lblBookType setFont:[UIFont fontWithName:self.lblBookType.font.fontName size:13.0f*SCREEN_XScale]];
        
        [self.lblFor setFont:[UIFont fontWithName:self.lblFor.font.fontName size:10.0f*SCREEN_XScale]];
        [self.lblTo setFont:[UIFont fontWithName:self.lblTo.font.fontName size:10.0f*SCREEN_XScale]];
        [self.lblAt setFont:[UIFont fontWithName:self.lblAt.font.fontName size:10.0f*SCREEN_XScale]];
        [self.lblAdminMsg setFont:[UIFont fontWithName:self.lblAdminMsg.font.fontName size:10.0f*SCREEN_XScale]];
        [self.lblFrom setFont:[UIFont fontWithName:self.lblFrom.font.fontName size:10.0f*SCREEN_XScale]];
        
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
