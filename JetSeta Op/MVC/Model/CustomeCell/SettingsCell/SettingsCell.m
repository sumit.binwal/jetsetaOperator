//
//  SettingsCell.m
//  JetSeta
//
//  Created by Suchita Bohra on 21/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import "SettingsCell.h"

@implementation SettingsCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.lblTxt.font = [UIFont fontWithName:@"Lato-Regular" size:27.0f];
    }
    else
    {
        self.lblTxt.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f*SCREEN_XScale];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
