//
//  LegListingCell.h
//  JetSeta Op
//
//  Created by Sumit Sharma on 14/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LegListingCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblUsd;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblAvlble;
@property (strong, nonatomic) IBOutlet UILabel *lbltime;
@property (strong, nonatomic) IBOutlet UILabel *lblMnth;
@property (strong, nonatomic) IBOutlet UILabel *lblCharter;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalCost;

@end
