//
//  AddAircraftCell.h
//  JetSeta Op
//
//  Created by Sumit Sharma on 13/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum : NSUInteger {
    CELL_TYPE_LABEL,
    CELL_TYPE_COLLECTION
} CELLTYPE;
@interface AircraftDetailCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblDiscription;

@property (strong, nonatomic) IBOutlet UILabel *lblTextFieldName;
@property(assign)CELLTYPE cellType;

@property (weak, nonatomic) IBOutlet UILabel *lblAircraftPhoto;
-(void)adjustCellWithType:(CELLTYPE)type;
@end
