//
//  SignupCellPhone.h
//  JetSeta
//
//  Created by Suchita Bohra on 14/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupCellPhone : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblPlaceHolderName;
@property (nonatomic,strong) IBOutlet UIButton *btnCountryCode;
@property (nonatomic, strong)IBOutlet UITextField *txtPhone;
@property (nonatomic, strong) IBOutlet UILabel *lblCountryCode;

@end
