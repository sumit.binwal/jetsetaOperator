//
//  WebViewVC.h
//  JetSeta
//
//  Created by Suchita Bohra on 20/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewVC : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView *webViewBrowse;
    UIActivityIndicatorView *indicator;
}

@property (nonatomic, strong) NSString *strNavigationTitle;
@property (nonatomic, strong) NSString *strUrl;

@end
