//
//  ChangePassVC.h
//  JetSeta
//
//  Created by Suchita Bohra on 20/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePassVC : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@end
