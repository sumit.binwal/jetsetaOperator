//
//  TutorialChildVC.h
//  JetSeta
//
//  Created by Suchita Bohra on 11/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialChildVC : UIViewController

@property (assign, nonatomic) NSInteger index;
@property (nonatomic,strong) IBOutlet UIImageView *imgViewTutorial;
@end
