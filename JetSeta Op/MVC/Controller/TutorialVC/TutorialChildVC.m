//
//  TutorialChildVC.m
//  JetSeta
//
//  Created by Suchita Bohra on 11/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import "TutorialChildVC.h"
//#import "JetSeta_Constants.h"

@interface TutorialChildVC ()
{
   NSMutableArray *arrImages;
    IBOutlet UILabel *lblPageCount;
}

@end

@implementation TutorialChildVC
@synthesize imgViewTutorial;
#pragma mark - ViewLifeCycle methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if ([UIScreen mainScreen].bounds.size.height==480) {
            arrImages=[[NSMutableArray alloc]initWithObjects:@"1_4.png",@"2_4.png",@"3_4.png",@"4_4.png",@"5_4.png", nil];
            
        }
        else if ([UIScreen mainScreen].bounds.size.height==568) {
            arrImages=[[NSMutableArray alloc]initWithObjects:@"1_5.png",@"2_5.png",@"3_5.png",@"4_5.png",@"5_5.png", nil];
        }
        else if ([UIScreen mainScreen].bounds.size.height==667) {
            arrImages=[[NSMutableArray alloc]initWithObjects:@"1_6.png",@"2_6.png",@"3_6.png",@"4_6.png",@"5_6.png", nil];
        }
        else if ([UIScreen mainScreen].bounds.size.height==736) {
            arrImages=[[NSMutableArray alloc]initWithObjects:@"1_6+.png",@"2_6+.png",@"3_6+.png",@"4_6+.png",@"5_6+.png", nil];
        }
        else
        {
            arrImages=[[NSMutableArray alloc]initWithObjects:@"1_6+.png",@"2_6+.png",@"3_6+.png",@"4_6+.png",@"5_6+.png", nil];
        }
    }
    else
    {
        if ([AppDelegate getSharedInstance].window.frame.size.height == 1024)
        {
            arrImages = [[NSMutableArray alloc]initWithObjects:@"i1_1.png",@"i1_2.png",@"i1_3.png",@"i1_4.png",@"i1_5.png", nil];
        }
        else  if ([AppDelegate getSharedInstance].window.frame.size.height == 1336)
        {
            arrImages = [[NSMutableArray alloc]initWithObjects:@"i2_1.png",@"i2_2.png",@"i2_3.png",@"i2_4.png",@"i2_5.png", nil];
        }
        else
        {
            arrImages = [[NSMutableArray alloc]initWithObjects:@"i2_1.png",@"i2_2.png",@"i2_3.png",@"i2_4.png",@"i2_5.png", nil];
        }
    }

    // Do any additional setup after loading the view from its nib.
//    FIX_IOS_7_LAY_OUT_ISSUE;
    
    

    if (self.index == 0)
    {
        [imgViewTutorial setImage:[UIImage imageNamed:arrImages[self.index]]];
    }
    else if (self.index == 1)
    {
        [imgViewTutorial setImage:[UIImage imageNamed:arrImages[self.index]]];
    }
    else if (self.index == 2)
    {
       [imgViewTutorial setImage:[UIImage imageNamed:arrImages[self.index]]];
    }
    else if (self.index == 3)
    {
       [imgViewTutorial setImage:[UIImage imageNamed:arrImages[self.index]]];
    }
    else if (self.index == 4)
    {
        [imgViewTutorial setImage:[UIImage imageNamed:arrImages[self.index]]];
    }
    
}

#pragma mark - MemoryManagement Methods

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
