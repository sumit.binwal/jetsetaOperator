//
//  AircraftDetailVC.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 20/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "AircraftDetailVC.h"
#import "AircraftDetailCell.h"
#import "ImageCollectionCell.h"
#import "UpdateAircraftVC.h"
#import "LoginVC.h"

@interface AircraftDetailVC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrTitles;
    IBOutlet UIPageControl *pgCntrol;
    IBOutlet UITableView *tblVw;
    NSMutableDictionary *dictData;
    IBOutlet UILabel *lblAircraftName;
    IBOutlet UILabel *lblLegCount;
    NSMutableArray *arrImage;
    NSMutableArray *arrImages;
        NSMutableArray *arrImagesIndex;
    IBOutlet UICollectionView *collctionImgVw;
    IBOutlet UILabel *lblleg;
    float intLastCellHeight;
    
    IBOutlet UIView *aircraftHeaderView;
    IBOutlet UIScrollView *scrollViewHeader;
    IBOutlet UIButton *btnLeft, *btnRight;
    
    IBOutlet UIPageControl *pageCtrl;
    NSArray *arrHeaderScrollContent;
    NSInteger pageCtrlCurrentPage;
}
@end

@implementation AircraftDetailVC
@synthesize strAircraftID;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)setUpView
{
    [CommonFunction setNavigationBar:self.navigationController];
    

    [self.navigationItem setTitle:@"Aircraft details"];
    UIBarButtonItem *barBtn1=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_icon"] style:UIBarButtonItemStyleDone target:self action:@selector(leftButtonClicked:)];
    
    [self.navigationItem setLeftBarButtonItem:barBtn1];
    
    UIBarButtonItem *barBtn2=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"AL_updateAircraft"] style:UIBarButtonItemStyleDone target:self action:@selector(rightButtonClicked:)];
    
    [self.navigationItem setRightBarButtonItem:barBtn2];
    
    arrTitles=[[NSMutableArray alloc]initWithObjects:@"AIRCRAFT TYPE",@"AIRCRAFT SPEED",@"BASE LOCATION",@"PRICE",@"NUMBER OF PASSENGERS",@"DESCRIPTION", nil];
    

    NSLog(@"=====%@",NSStringFromCGRect(aircraftHeaderView.frame));
    NSLog(@"=====%@",NSStringFromCGRect(scrollViewHeader.frame));
    if (IPAD) {
        tblVw.estimatedRowHeight = 144*SCREEN_YScale;
        tblVw.rowHeight = UITableViewAutomaticDimension;
        [lblAircraftName setFont:[UIFont fontWithName:lblAircraftName.font.fontName size:27.0f]];
        [lblleg setFont:[UIFont fontWithName:lblleg.font.fontName size:15.0f]];
        [lblLegCount setFont:[UIFont fontWithName:lblLegCount.font.fontName size:27.0f]];
        
    }
    else
    {
        tblVw.estimatedRowHeight = 44*SCREEN_YScale;
        tblVw.rowHeight = UITableViewAutomaticDimension;
    }
    
    
    [collctionImgVw registerNib:[UINib nibWithNibName:NSStringFromClass([ImageCollectionCell class]) bundle:nil] forCellWithReuseIdentifier:@"ImageCollectionCell"];
    
    }
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    APPDELEGATE.mainTapBarCntroller.tabBar.hidden=YES;
    
    if ([CommonFunction reachabiltyCheck]) {
        [CommonFunction showActivityIndicatorWithText:@""];
        [self getAircraftDetail];
    }
    else
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please check your network connection. " onViewController:self useAsDelegate:NO dismissBlock:nil];
    }
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];

}
#pragma mark - User Define Delegate Method


#pragma mark - IBAction Methods
- (IBAction)leftArrowBtnClicked:(UIButton *)sender {
    
    if (pgCntrol.currentPage==arrImage.count) {
        
    }
    else
    {
        if (pgCntrol.currentPage<=0) {
            
        }
        else
        {
            [collctionImgVw setContentOffset:CGPointMake(collctionImgVw.frame.size.width*(pgCntrol.currentPage-1), 0) animated:YES];
        }
        
        
    }
}
- (IBAction)rightArrowBtnClicked:(UIButton *)sender {
    
    if (pgCntrol.currentPage==arrImage.count-1) {
        
    }
    else
    {
        [collctionImgVw setContentOffset:CGPointMake(collctionImgVw.frame.size.width*(pgCntrol.currentPage+1), 0) animated:YES];
        
    }
    
}

-(IBAction)leftButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)rightButtonClicked:(id)sender
{
    
    
    UpdateAircraftVC *uavc=[[UpdateAircraftVC alloc]initWithNibName:NSStringFromClass([UpdateAircraftVC class]) bundle:nil];
    uavc.dataDict=dictData;
    uavc.arrImagesIndex=arrImagesIndex;
    uavc.arrImages=arrImages;
    [self presentViewController:uavc animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UICollection View Delegate Method
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrImages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectioView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImageCollectionCell   *cell = [collectioView dequeueReusableCellWithReuseIdentifier:@"ImageCollectionCell" forIndexPath:indexPath];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(ImageCollectionCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    pgCntrol.currentPage=indexPath.row;
    [cell.imgVw setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",arrImages[indexPath.row][@"image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(self.view.frame.size.width,150*SCREEN_YScale);
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - TableView Delegate Method

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return   arrTitles.count;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    if (indexPath.row==5) {
//    
//            return intLastCellHeight*SCREEN_YScale;
//    }
//    else
//    {
//    return 44*SCREEN_YScale;
//    }
//    
//}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString * cellIdentifier=@"AircraftDetailCell";
    
    
    AircraftDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (!cell)
    {
        cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([AircraftDetailCell class]) owner:self options:nil][0];
    }
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(AircraftDetailCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        cell.lblTextFieldName.text=[arrTitles objectAtIndex:indexPath.row];
    cell.lblDiscription.tag=indexPath.row;
    if (indexPath.row==0) {
        cell.lblDiscription.text=[NSString stringWithFormat:@"%@",[[dictData valueForKey:@"aircraft_type"]capitalizedString]
                                  ];
        cell.lblDiscription.textColor=[UIColor colorWithRed:28.0f/255.0f green:126.0f/255.0f blue:231.0f/255.0f alpha:1];
    }
    switch (indexPath.row) {
        case 1:
        {
            cell.lblDiscription.text=[NSString stringWithFormat:@"%@ km/hr",[dictData valueForKey:@"aircraft_speed"]];
            cell.lblDiscription.textColor=[UIColor colorWithRed:28.0f/255.0f green:126.0f/255.0f blue:231.0f/255.0f alpha:1];
            break;
        }
        case 2:
        {
            cell.lblDiscription.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"aircraft_address"]];
            cell.lblDiscription.textColor=[UIColor colorWithRed:28.0f/255.0f green:126.0f/255.0f blue:231.0f/255.0f alpha:1];
            break;
        }
        case 3:
        {
            cell.lblDiscription.text=[NSString stringWithFormat:@"USD %@/hr",[dictData valueForKey:@"price_per_hour"]];
            cell.lblDiscription.textColor=[UIColor colorWithRed:28.0f/255.0f green:126.0f/255.0f blue:231.0f/255.0f alpha:1];
            break;
        }
        case 4:
        {
            cell.lblDiscription.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"capacity"]];
            cell.lblDiscription.textColor=[UIColor colorWithRed:28.0f/255.0f green:126.0f/255.0f blue:231.0f/255.0f alpha:1];
            break;
        }
        case 5:
        {
            cell.lblDiscription.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"aircraft_description"]];
            cell.lblDiscription.textColor=[UIColor colorWithRed:59.0f/255.0f green:59.0f/255.0f blue:59.0f/255.0f alpha:1];
            
            
           
            

            
            break;
            
        }
    }
    //For Getting url from uiimage
    if (!(dictData.count>0)) {
    
        cell.lblDiscription.text=@"";
    }

    
}
- (CGSize)sizeForLabel:(UILabel *)label {
    CGSize constrain = CGSizeMake(label.bounds.size.width, FLT_MAX);
    CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constrain lineBreakMode:UILineBreakModeWordWrap];

    return size;
}

#pragma mark - WebService API
-(void)getAircraftDetail
{
    
    NSString *url = [NSString stringWithFormat:@"aircraft_detail/%@/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN],strAircraftID];
    //    http://192.168.0.131:7684/charter_detail/83a595e384926272ad5cf8a48208d486/2
    
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
        }
        
        else if([operation.response statusCode]==200)
            
        {
            if ([[responseDict objectForKey:@"type"] boolValue]) {
                dictData = [responseDict objectForKey:@"aircraft"];
                lblAircraftName.text=[[dictData valueForKey:@"aircraft_name"] uppercaseString];
                arrImage=[[NSMutableArray alloc]init];
                if ([[dictData objectForKey:@"total_legs"]intValue]>1) {
                    lblleg.text=@"TRIPS";
                }
                else
                {
                    lblleg.text=@"TRIP";
                }
                int lblLegCounts=[[dictData objectForKey:@"total_legs"]intValue];
                if (lblLegCounts>0) {
                lblLegCount.text=[NSString stringWithFormat:@"%02d",lblLegCounts];
                }
                else
                {
                                    lblLegCount.text=@"0";
                }

                arrImage=[[responseDict objectForKey:@"aircraft"] objectForKey:@"images"];
                pgCntrol.numberOfPages=arrImage.count;
                
    

                arrImages=[[NSMutableArray alloc]init];
                arrImages=[arrImage mutableCopy];
                NSArray *tempArray= [arrImages sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"id" ascending:YES]]];
                [arrImages removeAllObjects];
                [arrImages addObjectsFromArray:[tempArray mutableCopy]];
                [collctionImgVw reloadData];
//                NSMutableArray *tempArry=[[NSMutableArray alloc]init];
//                arrImages=[[NSMutableArray alloc]init];
//                arrImagesIndex=[[NSMutableArray alloc]init];
//                tempArry=[arrImage mutableCopy];
//                
//                    for (NSInteger i=0; i<tempArry.count; i++) {
//                
////
//                        [self downloadImageWithUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@",tempArry[i][@"image"]]] order:[tempArry[i][@"id"]integerValue] completeion:^(UIImage *img, NSInteger orderNumber) {
//                            [arrImages addObject:@{@"image":img, @"order":[NSNumber numberWithInteger:orderNumber]}];
//                            
//                        }];
//                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//                            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",tempArry[i][@"image"]]]];
//                            UIImage *img = [UIImage imageWithData:imageData];
//                            
//                            [arrImages addObject:img];
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                // Update the UI
//                            NSLog(@"=======================%d",i);
//                                [arrImagesIndex addObject:tempArry[i][@"id"]];
//                            });
//                        });
//                    }
                

                
                
                UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0)];
                lbl.numberOfLines=0;
                lbl.text=[dictData valueForKey:@"aircraft_description"];
                
                intLastCellHeight=[self sizeForLabel:lbl].height / lbl.font.lineHeight;
                if (IPAD) {
                intLastCellHeight=intLastCellHeight*30.0f;
                }
                else
                {
                intLastCellHeight=intLastCellHeight*15.0f;
                }
                
              //  [tblVw layoutIfNeeded];
                [tblVw reloadData];
                         
                
            }
            else
            {
                [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          [CommonFunction removeActivityIndicator];
                                          
                                          if([operation.response statusCode]  == 426)
                                          {
                                              NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                              NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                              [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                              }];
                                          }

                                          else if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  
                                                  
                                                  [self getAircraftDetail];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}


- (void)downloadImageWithUrl:(NSURL *)url order:(NSInteger )orderNumber completeion:(void(^)(UIImage* img,NSInteger orderNumber)) completion
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        UIImage *img = [UIImage imageWithData:imageData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(img,orderNumber);
        });
    });
}

@end
