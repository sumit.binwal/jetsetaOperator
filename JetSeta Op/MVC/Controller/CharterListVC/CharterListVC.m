//
//  CharterListVC.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 15/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "CharterListVC.h"
#import "LegListingVC.h"
#import "AircraftListCell.h"
#import "NotificationVC.h"
#import "AddCharterVC.h"
#import <Crashlytics/Crashlytics.h>
#import "LoginVC.h"

@interface CharterListVC ()<UITableViewDataSource,UITableViewDelegate>
{
    
    IBOutlet UILabel *lblErrorMsg;
    NSMutableArray *arrCharterList;
    IBOutlet UITableView *tblCharterListing;
    IBOutlet UIButton *btnCreateCharter;
    IBOutlet UILabel *lblMsgCreateCharter;
    int currentPage,totalpost;
    BOOL isNextPage;

}
@end

@implementation CharterListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    
    [APPDELEGATE.mainTapBarCntroller.tabBar setHidden:NO];
    
    // Do any additional setup after loading the view from its nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupView
{
    [CommonFunction setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"My Charter"];
    
    SWRevealViewController *revealController = [self revealViewController];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"AL_Menu"]style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
//    
//    UIBarButtonItem *barBtn1=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"AL_Menu"] style:UIBarButtonItemStyleDone target:self action:@selector(menuButtonClicked:)];
//    
//    [self.navigationItem setLeftBarButtonItem:barBtn1];
    
    
    UIBarButtonItem *barBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"AL_AddButton"] style:UIBarButtonItemStyleDone target:self action:@selector(addButtonClicked:)];
    
    [self.navigationItem setRightBarButtonItem:barBtn];
    
    if(IPAD)
    {
        btnCreateCharter.titleLabel.font=[UIFont fontWithName:btnCreateCharter.titleLabel.font.fontName size:24];
        [lblMsgCreateCharter setFont:[UIFont fontWithName:lblMsgCreateCharter.font.fontName size:30]];
        
    }
    else
    {
        
        btnCreateCharter.titleLabel.font=[UIFont fontWithName:btnCreateCharter.titleLabel.font.fontName size:btnCreateCharter.titleLabel.font.pointSize*SCREEN_XScale];
        [lblMsgCreateCharter setFont:[UIFont fontWithName:lblMsgCreateCharter.font.fontName size:lblMsgCreateCharter.font.pointSize*SCREEN_XScale]];
    }
    
    
}
#pragma mark - UserDefine Method


-(IBAction)menuButtonClicked:(id)sender
{
    
}

-(IBAction)addButtonClicked:(id)sender
{
    NSLog(@"%@",[NSUSERDEFAULTS valueForKey:kPUBLISH_AIRCRAFT]);
    if (![[NSUSERDEFAULTS valueForKey:kPUBLISH_AIRCRAFT]boolValue]) {
        NSLog(@"%d",[[NSUSERDEFAULTS valueForKey:kPUBLISH_COUNT] intValue]);
        if ([[NSUSERDEFAULTS valueForKey:kPUBLISH_COUNT] intValue]>0) {
            AddCharterVC *acvc=[[AddCharterVC alloc]initWithNibName:NSStringFromClass([AddCharterVC class]) bundle:nil];
            //     LegListingVC *acvc=[[LegListingVC alloc]initWithNibName:NSStringFromClass([LegListingVC class]) bundle:nil];
            acvc.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:acvc animated:YES];

        }
        else
        {
        [CommonFunction showAlertWithTitle:@"" message:@"Your aircraft is awaiting approval by the admin team." onViewController:self useAsDelegate:NO dismissBlock:nil];
        }
    }
    else
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please add aircraft." onViewController:self useAsDelegate:NO dismissBlock:nil];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.revealViewController.frontViewController.view setUserInteractionEnabled:YES];
    [APPDELEGATE.mainTapBarCntroller.tabBar setHidden:NO];
    currentPage=1;
    isNextPage=1;

    if ([CommonFunction reachabiltyCheck]) {
        [CommonFunction showActivityIndicatorWithText:@""];
        [self getCharterListAPI];
    }
    else
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please check your network connection. " onViewController:self useAsDelegate:NO dismissBlock:nil];
    }
}

#pragma mark - TableView Delegate Method

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return   arrCharterList.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 208*SCREEN_YScale;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * cellIdentifier=@"AircraftListCell";
    
    AircraftListCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[[AircraftListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(AircraftListCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.vwLegBg.layer.cornerRadius=3.0f;
    cell.lblLocation.text=arrCharterList[indexPath.row][@"address"];
    cell.lblDiscription.text=[arrCharterList[indexPath.row][@"aircraft_type"]capitalizedString];
    cell.lblNumberofUser.text=[NSString stringWithFormat:@"%@",arrCharterList[indexPath.row][@"capacity"]];
    
    if ([arrCharterList[indexPath.row][@"legs_count"]intValue]>1) {
        cell.lblLeg.text=[NSString stringWithFormat:@"%@ TRIPS",arrCharterList[indexPath.row][@"legs_count"]];;
    }
    else
    {
        cell.lblLeg.text=[NSString stringWithFormat:@"%@ TRIP",arrCharterList[indexPath.row][@"legs_count"]];
    }
    
    cell.lblTitle.text=arrCharterList[indexPath.row][@"name"];
    cell.lblPrice.text=[NSString stringWithFormat:@"%@",arrCharterList[indexPath.row][@"price_per_hour"]];
    [cell.imgAircraftImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",arrCharterList[indexPath.row][@"image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    if (indexPath.row == [arrCharterList count]-1)
    {
//         [CommonFunction showActivityIndicatorWithText:@""];
        [self getCharterListAPI];
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LegListingVC *llvc=[[LegListingVC alloc]initWithNibName:NSStringFromClass([LegListingVC class]) bundle:nil];
    llvc.strCharterID=arrCharterList[indexPath.row][@"charters_id"];
    llvc.isComesFromMyCharterListing = YES;
    [self.navigationController pushViewController:llvc animated:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ((touch.view == self.view))
    {
        return NO;
    }
    return YES;
}
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
//{
//    NSLog(@"%@",NSStringFromClass([touch.view class]));
//    return [NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"];
//}
#pragma mark - WebService APi
-(void)getCharterListAPI
{
    if (currentPage==1) {
        if (!isNextPage) {
            [CommonFunction removeActivityIndicator];
            
            return;
        }
    }
    else
    {
        if(totalpost==[arrCharterList count])
        {
            [CommonFunction removeActivityIndicator];
            return;
        }
    }
    NSString *url = [NSString stringWithFormat:@"list_charters/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN]];
    //    http://192.168.0.131:7684/list_charters/83a595e384926272ad5cf8a48208d486
    
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    NSMutableDictionary *param =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithUnsignedInteger:currentPage],@"page", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
        }
        
        else if([operation.response statusCode]==200)
        {
            
                NSArray *arrListing = [responseDict valueForKey:@"charters"];
                if (arrListing.count>0) {
                    [btnCreateCharter setHidden:YES];
                    [lblMsgCreateCharter setHidden:YES];
                    lblErrorMsg.text=@"";
                    
                    
                    
                    //                arrNotificatinFeeds=[responseDict objectForKey:@"data"];
                    
                    if (arrListing.count > 0)
                    {
                        if (currentPage == 1)
                        {
                            if (arrCharterList.count > 0)
                            {
                                [arrCharterList removeAllObjects];
                            }
                            if (arrCharterList == nil)
                            {
                                arrCharterList = [NSMutableArray array];
                            }
                            arrCharterList = [arrListing mutableCopy];
                        }
                        else
                        {
                            [arrCharterList addObjectsFromArray:arrListing];
                        }
                    }
                    
                    [tblCharterListing reloadData];
                    
                    isNextPage=[[responseDict objectForKey:@"nextpage"]boolValue];
                    if ([[responseDict objectForKey:@"nextpage"]boolValue]) {
                        currentPage++;
                    }
                    else
                    {
                        totalpost=(int)arrCharterList.count;
                        return;
                    }
                    
                }
                else
                {
                    [arrCharterList removeAllObjects];
                    [tblCharterListing reloadData];
                    lblErrorMsg.text=@"";
                    [btnCreateCharter setHidden:NO];
                    [lblMsgCreateCharter setHidden:NO];
                }
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                                    [CommonFunction removeActivityIndicator];
                                          
                                          if([operation.response statusCode]  == 426)
                                          {
                                              NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                              NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                              [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                              }];
                                          }

                                          else if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  
                                                  
                                                  [self getCharterListAPI];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
