//
//  UtilityClass.h
//  Tabco App
//
//  Created by Santosh on 17/12/15.
//  Copyright © 2015 Vaibhav Khatri. All rights reserved.
//


#pragma mark -
#pragma mark CLASS INTERFACE
@interface UtilityClass : NSObject

#pragma mark -
+ (UIImage *)convertBase64StringToImage:(NSString *)base64String;
#pragma mark -
+ (NSString *)convertImageToBase64String:(UIImage *)image;
#pragma mark -
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController useAsDelegate:(BOOL)isDelegate dismissBlock:(void(^)())dismissBlock;
#pragma mark -
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController useAsDelegate:(BOOL)isDelegate withButtonsArray:(NSArray *)buttonsArray dismissBlock:(void(^)(NSInteger buttonIndex))dismissBlock;
#pragma mark -
+ (void)saveUserInfoWithDictionary:(NSDictionary *)dictionary;
#pragma mark -
+ (NSDictionary *)getUserInfoDictionary;
#pragma mark -
+ (void)deleteUserInfoDictionary;
#pragma mark -
+(NSMutableArray *)convertImageBytesToArrayUsingImage:(UIImage *)image;
#pragma mark -
+(BOOL)IsValidEmail:(NSString *)checkString;
+ (BOOL)isValueNotEmpty:(NSString*)aString;
+ (BOOL)isValidMobileNumber:(NSString*)number;
#pragma mark -
+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize;
#pragma mark -
+ (NSArray *)getCountryCodesWithName;

@end
