//
//  ImageCapture.h
//  emptyApp
//
//  Created by Suchita on 06/09/14.
//  Copyright (c) 2014 Suchita. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, kSourceType) {
    kSourceTypeCamera      = 0,    // Camera
    kSourceTypeLibrary     = 1    // Library
};

@interface ImageCapture : NSObject<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

- (id) initWithSourceType:(kSourceType) sourceType;

- (void) presentImagePickerOnController:(id) controller onDidFinish:(void (^) (NSDictionary *imageInfo)) finish onCancel:(void (^) (void)) cancel;
@end
