//
//  ConnectionManager.m
//  Tempus
//
//  Created by Ashish Sharma on 10/07/14.
//  Copyright (c) 2014 KT. All rights reserved.
//

#import "ConnectionManager.h"

@implementation ConnectionManager

+ (ConnectionManager *) sharedInstance
{
    static ConnectionManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ConnectionManager alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

- (void) startRequestWithHttpMethod:(kHttpMethodType) httpMethodType withHttpHeaders:(NSMutableDictionary*) headers withServiceName:(NSString*) serviceName withParameters:(NSMutableDictionary*) params withSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success withFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
    [headers setObject:version forKey:@"Accept"];
    [headers setObject:@"ios" forKey:@"os"];
    NSString *serviceUrl = [serverURL stringByAppendingString:serviceName];
    NSLog(@"%@",serviceUrl);
    
//    NSString * apiURLStr;
//    
//    if (IS_RETINA) {
//        apiURLStr = [serviceUrl stringByAppendingFormat:@"/agent:2"];
//    }
//    else{
//        apiURLStr = [serviceUrl stringByAppendingFormat:@"agent:1"];
//    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    if (headers != nil)
    {
        NSArray *allHeaders = [headers allKeys];
        
        for (NSString *key in allHeaders)
        {
            [manager.requestSerializer setValue:[headers objectForKey:key] forHTTPHeaderField:key];
        }
    }
    
    [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString *(NSURLRequest *request, NSDictionary *parameters, NSError *__autoreleasing *error) {
        __block NSMutableString *query = [NSMutableString stringWithString:@""];
        
        NSError *err;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
        NSMutableString *jsonString = [[NSMutableString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSLog(@"json string %@",jsonString);
        
        query = jsonString;
        return query;
    }];
    
    switch (httpMethodType)
    {
        case kHttpMethodTypeGet:
        {
            [manager GET:serviceUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {

                NSLog(@"JSON: %@", responseObject);
                
                if (success != nil)
                {
                   success(operation,responseObject);
                    
                //    NSDictionary *dic = responseObject
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                NSLog(@"Error: %@", error);
                
                if (failure != nil)
                {
                    failure(operation,error);
                }
            }];
        }
            break;
        case kHttpMethodTypePost:
        {
            [manager POST:serviceUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSLog(@"url: %@", serviceUrl);
                NSLog(@"JSON: %@", responseObject);

                
                if (success != nil)
                {
                    success(operation,responseObject);
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                NSLog(@"Error: %@", error);
                
                if (failure != nil)
                {
                    failure(operation,error);
                }
            }];
        }
            break;
        case kHttpMethodTypeDelete:
        {            
            [manager DELETE:serviceUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSLog(@"JSON: %@", responseObject);
                
                if (success != nil)
                {
                    success(operation,responseObject);
                }
                
            }
                failure:^(AFHTTPRequestOperation *operation, NSError *error)
            {
                
                NSLog(@"Error: %@", error);
                
                if (failure != nil)
                {
                    failure(operation,error);
                }
                
            }];

        }
            break;
        case kHttpMethodTypePut:
        {
            [manager PUT:serviceUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSLog(@"JSON: %@", responseObject);
                
                if (success != nil)
                {
                    success(operation,responseObject);
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                NSLog(@"Error: %@", error);
                
                if (failure != nil)
                {
                    failure(operation,error);
                }
                
            }];    
        }
            break;
            
        default:
            break;
    }
}

@end
